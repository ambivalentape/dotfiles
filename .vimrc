cal plug#begin('~/.vim/plugged')

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-speeddating'
Plug 'dracula/vim'
Plug 'jlanzarotta/bufexplorer'
Plug 'mhinz/vim-startify'
Plug 'idris-hackers/idris-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'scrooloose/nerdtree'
Plug 'lervag/vimtex'
Plug 'Valloric/YouCompleteMe'
Plug 'Shougo/vimproc.vim', {'do' : 'make'}
Plug 'w0rp/ale'
Plug 'eagletmt/ghcmod-vim'
"Scala related plugins"
"Plug 'ensime/ensime-vim'"
Plug 'derekwyatt/vim-scala'
Plug 'hellerve/carp-vim'
Plug 'elmcast/elm-vim'
Plug 'raichoo/purescript-vim'
Plug 'JuliaEditorSupport/julia-vim'
Plug 'jpalardy/vim-slime'
Plug 'zyedidia/julialint.vim'
Plug 'leafgarland/typescript-vim'
Plug 'jceb/vim-orgmode'
call plug#end()
set encoding=utf-8
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*

"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#ale#enabled = 1

" Show just the filename
"let g:airline#extensions#tabline#fnamemod = ':t'

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

let ensime_server_v2=1
"autocmd BufWritePost *.scala silent :EnTypeCheck
"nnoremap <localleader>t :EnTypeCheck<CR>

nnoremap <Leader>ht :GhcModType<cr>
nnoremap <Leader>htc :GhcModTypeClear<cr>
autocmd FileType haskell nnoremap <buffer> <leader>? :call ale#cursor#ShowCursorDetail()<cr>


"Splits"
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
set splitbelow
set splitright

syntax on
filetype on
filetype plugin indent on
color dracula
let g:powerline_pycmd="py3"
let g:pymcd_powerline="py3"

set laststatus=2 " Always display the statusline in all windows
set showtabline=2 " Always display the tabline, even if there is only one tab
set noshowmode " Hide the default mode text (e.g. -- INSERT -- below the statusline)
set t_Co=256
let g:org_todo_keywords=['TODO', 'DOING', '|', 'DONE']
set timeoutlen=2000

let purescript_indent_if = 3
set nomodeline
