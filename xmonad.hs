import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.EwmhDesktops(fullscreenEventHook,ewmh)
import XMonad.Hooks.ManageHelpers
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import System.IO

import Graphics.X11.ExtraTypes.XF86



main = do
    xmproc <- spawnPipe "xmobar"

    xmonad $ ewmh $ defaultConfig
        { manageHook = manageDocks <+> (isFullscreen --> doFullFloat) <+> manageHook defaultConfig
        , layoutHook = avoidStruts  $  layoutHook defaultConfig
	, handleEventHook = handleEventHook defaultConfig <+> docksEventHook
        , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
			, ppHiddenNoWindows = xmobarColor "grey" ""
                        }
        , modMask = mod4Mask     -- Rebind Mod to the Windows key
        } `additionalKeys`
        [ ((mod4Mask .|. shiftMask, xK_z), spawn "xscreensaver-command -lock; xset dpms force off")
        , ((controlMask, xK_Print), spawn "sleep 0.2; scrot -s")
        , ((0, xK_Print), spawn "scrot")
        , ((mod4Mask, xK_b), sendMessage ToggleStruts)
	, ((controlMask, xK_F6), spawn "pactl set-sink-volume @DEFAULT_SINK@ +1.5%") 
	, ((controlMask, xK_F5), spawn "pactl set-sink-volume @DEFAULT_SINK@ -1.5%") 
	, ((controlMask, xK_F3), spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle") 
        ]
